package ltd.vblago.taskfp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ltd.vblago.taskfp1.fragment.CountryFragment;
import ltd.vblago.taskfp1.fragment.RegionFragment;
import ltd.vblago.taskfp1.fragment.SubRegionFragment;
import ltd.vblago.taskfp1.fragment.SubRegionsListFragment;
import ltd.vblago.taskfp1.model.ActivityCommunication;

public class MainActivity extends AppCompatActivity
        implements ActivityCommunication {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, RegionFragment.newInstance())
                .commit();
    }

    @Override
    public void goToSubRegionsListFragment(String region) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, SubRegionsListFragment.newInstance(region))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void goToSubRegionFragment(String subRegion) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, SubRegionFragment.newInstance(subRegion))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void goToCountryFragment(String country) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, CountryFragment.newInstance(country))
                .addToBackStack(null)
                .commit();
    }
}
