package ltd.vblago.taskfp1.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ltd.vblago.taskfp1.R;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder>{

    public List<String> list;
    private Context context;
    private OnListClick onListClick;

    public RecyclerAdapter(Context context, List<String> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setOnListClick(OnListClick onListClick) {
        this.onListClick = onListClick;
    }

    public String getItem(int position) {
        return list.get(position);
    }

    public interface OnListClick {
        void onItemClick(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name_item)
        TextView name;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(String str) {
            name.setText(str);
        }

        @OnClick(R.id.list_item)
        void onItemClick() {
            if (onListClick != null) {
                onListClick.onItemClick(getAdapterPosition());
            }

        }

    }

}
