package ltd.vblago.taskfp1.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ltd.vblago.taskfp1.R;
import ltd.vblago.taskfp1.model.Country;
import ltd.vblago.taskfp1.util.Retrofit;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CountryFragment extends Fragment {

    String countryStr;
    Unbinder unbinder;
    @BindView(R.id.image_view)
    ImageView imageView;
    @BindView(R.id.name_view) TextView nameView;
    @BindView(R.id.alpha2Code_view) TextView alpha2CodeView;
    @BindView(R.id.nativeName_view) TextView nativeNameView;
    @BindView(R.id.region_view) TextView regionView;
    @BindView(R.id.subRegion_view) TextView subRegionView;
    @BindView(R.id.area_view) TextView areaView;
    @BindView(R.id.numericCode_view) TextView numericCodeView;
    @BindView(R.id.currency_view) TextView currencyView;

    public static CountryFragment newInstance(String country) {
        CountryFragment fragment = new CountryFragment();
        Bundle args = new Bundle();
        args.putString("country", country);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            countryStr = getArguments().getString("country");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_country, container, false);
        unbinder = ButterKnife.bind(this, root);

        Retrofit.getCountry("country", countryStr, new Callback<Country>() {
            @Override
            public void success(Country country, Response response) {
                setCountryInfo(country);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        });
        return root;
    }

    private void setCountryInfo(Country country){
        Picasso.with(getContext())
                .load(country.flagPng)
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)
                .into(imageView);
        nameView.setText(country.name);
        alpha2CodeView.setText(String.format("AlphaCodes: %s  %s", country.alpha2Code, country.alpha3Code));
        nativeNameView.setText(String.format("Native name and language: %s, %s", country.nativeName, country.nativeLanguage));
        regionView.setText(String.format("Region: %s", country.region));
        subRegionView.setText(String.format("Subregion: %s", country.subRegion));
        areaView.setText(String.format(Locale.ENGLISH,"Area and coordinates: %.2f, %.2f, %.2f", country.area, country.latitude, country.longitude));
        numericCodeView.setText(String.format(Locale.ENGLISH,"Numeric code: %d", country.numericCode));
        currencyView.setText(String.format("Currency: %s  %s  %s", country.currencyName, country.currencySymbol, country.currencyCode));
    }

}
