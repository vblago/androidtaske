package ltd.vblago.taskfp1.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ltd.vblago.taskfp1.R;
import ltd.vblago.taskfp1.adapter.RecyclerAdapter;
import ltd.vblago.taskfp1.model.ActivityCommunication;
import ltd.vblago.taskfp1.util.ReadTask;
import ltd.vblago.taskfp1.util.Retrofit;
import ltd.vblago.taskfp1.util.WriteTask;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RegionFragment extends Fragment {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    ActivityCommunication activityCommunication;
    Unbinder unbinder;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ActivityCommunication) {
            activityCommunication = (ActivityCommunication) context;
        } else {
            throw new RuntimeException(Context.class.getSimpleName() + " must implement ActivityCommunication interface");
        }
    }

    public static RegionFragment newInstance() {
        return new RegionFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_region, container, false);
        unbinder = ButterKnife.bind(this, root);

        ReadTask readTask = new ReadTask(getContext(), "regions");
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        if (!readTask.fileExist()) {
            Retrofit.getInfoOneParameter("regions_list", new Callback<List<String>>() {
                @Override
                public void success(final List<String> regions, Response response) {
                    final RecyclerAdapter recyclerAdapter = new RecyclerAdapter(getContext(), regions);
                    recyclerView.setAdapter(recyclerAdapter);
                    recyclerAdapter.setOnListClick(new RecyclerAdapter.OnListClick() {
                        @Override
                        public void onItemClick(int position) {
                            activityCommunication.goToSubRegionsListFragment(recyclerAdapter.getItem(position));
                        }
                    });
                    WriteTask writeTask = new WriteTask(getContext(), "regions", recyclerAdapter.list);
                    writeTask.execute();
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            readTask.setReadCallback(new ReadTask.ReadCallback() {
                @Override
                public void saveObject(Object object) {
                    List<String> regions = (List<String>) object;
                    final RecyclerAdapter recyclerAdapter = new RecyclerAdapter(getContext(), regions);

                    recyclerView.setAdapter(recyclerAdapter);
                    recyclerAdapter.setOnListClick(new RecyclerAdapter.OnListClick() {
                        @Override
                        public void onItemClick(int position) {
                            activityCommunication.goToSubRegionsListFragment(recyclerAdapter.getItem(position));
                        }
                    });
                }
            });
            readTask.execute();
        }
        return root;
    }

}
