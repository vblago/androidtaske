package ltd.vblago.taskfp1.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ltd.vblago.taskfp1.R;
import ltd.vblago.taskfp1.adapter.RecyclerAdapter;
import ltd.vblago.taskfp1.model.ActivityCommunication;
import ltd.vblago.taskfp1.util.Retrofit;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubRegionFragment extends Fragment {

    String subRegion;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    ActivityCommunication activityCommunication;
    RecyclerAdapter recyclerAdapter;
    Unbinder unbinder;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ActivityCommunication) {
            activityCommunication = (ActivityCommunication) context;
        } else {
            throw new RuntimeException(Context.class.getSimpleName() + " must implement ActivityCommunication interface");
        }
    }

    public static SubRegionFragment newInstance(String subRegion) {
        SubRegionFragment fragment = new SubRegionFragment();
        Bundle args = new Bundle();
        args.putString("subRegion", subRegion);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            subRegion = getArguments().getString("subRegion");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_sub_region, container, false);
        unbinder = ButterKnife.bind(this, root);

        Retrofit.getInfoTwoParameters("subregion_countries", subRegion, new Callback<List<String>>() {
            @Override
            public void success(final List<String> countries, Response response) {
                recyclerAdapter = new RecyclerAdapter(getContext(), countries);
                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(recyclerAdapter);
                recyclerAdapter.setOnListClick(new RecyclerAdapter.OnListClick() {
                    @Override
                    public void onItemClick(int position) {
                        activityCommunication.goToCountryFragment(recyclerAdapter.getItem(position));
                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        });
        return root;
    }

}
