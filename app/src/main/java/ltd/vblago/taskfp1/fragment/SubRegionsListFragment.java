package ltd.vblago.taskfp1.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ltd.vblago.taskfp1.R;
import ltd.vblago.taskfp1.adapter.RecyclerAdapter;
import ltd.vblago.taskfp1.model.ActivityCommunication;
import ltd.vblago.taskfp1.util.ReadTask;
import ltd.vblago.taskfp1.util.Retrofit;
import ltd.vblago.taskfp1.util.WriteTask;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SubRegionsListFragment extends Fragment {

    String region;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    ActivityCommunication activityCommunication;
    Unbinder unbinder;
    RecyclerAdapter recyclerAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ActivityCommunication) {
            activityCommunication = (ActivityCommunication) context;
        } else {
            throw new RuntimeException(Context.class.getSimpleName() + " must implement ActivityCommunication interface");
        }
    }

    public static SubRegionsListFragment newInstance(String region) {
        SubRegionsListFragment fragment = new SubRegionsListFragment();
        Bundle args = new Bundle();
        args.putString("region", region);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            region = getArguments().getString("region");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_sub_regions_list, container, false);
        unbinder = ButterKnife.bind(this, root);

        ReadTask readTask = new ReadTask(getContext(), region);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        if (!readTask.fileExist()) {
            Retrofit.getInfoTwoParameters("subregions_list", region, new Callback<List<String>>() {
                @Override
                public void success(final List<String> subRegions, Response response) {
                    recyclerAdapter = new RecyclerAdapter(getContext(), subRegions);
                    recyclerView.setAdapter(recyclerAdapter);
                    recyclerAdapter.setOnListClick(new RecyclerAdapter.OnListClick() {
                        @Override
                        public void onItemClick(int position) {
                            activityCommunication.goToSubRegionFragment(recyclerAdapter.getItem(position));
                        }
                    });
                    WriteTask writeTask = new WriteTask(getContext(), region, recyclerAdapter.list);
                    writeTask.execute();
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                }
            });
        }else {
            readTask.setReadCallback(new ReadTask.ReadCallback() {
                @Override
                public void saveObject(Object object) {
                    List<String> subRegions = (List<String>) object;
                    recyclerAdapter = new RecyclerAdapter(getContext(), subRegions);

                    recyclerView.setAdapter(recyclerAdapter);
                    recyclerAdapter.setOnListClick(new RecyclerAdapter.OnListClick() {
                        @Override
                        public void onItemClick(int position) {
                            activityCommunication.goToSubRegionFragment(recyclerAdapter.getItem(position));
                        }
                    });
                }
            });
            readTask.execute();
        }
        return root;
    }

}
