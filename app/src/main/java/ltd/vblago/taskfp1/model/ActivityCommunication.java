package ltd.vblago.taskfp1.model;

public interface ActivityCommunication {
    void goToSubRegionsListFragment(String region);
    void goToSubRegionFragment(String subRegion);
    void goToCountryFragment(String country);
}
