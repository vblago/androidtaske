package ltd.vblago.taskfp1.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Country implements Parcelable{
    public String name;
    public String alpha2Code;
    public String alpha3Code;
    public String nativeName;
    public String region;
    public String subRegion;
    public float latitude;
    public float longitude;
    public float area;
    public int numericCode;
    public String nativeLanguage;
    public String currencyCode;
    public String currencyName;
    public String currencySymbol;
    public String flagPng;

    public Country(String name, String alpha2Code, String alpha3Code, String nativeName, String region, String subRegion, String latitude, String longitude, String area, String numericCode, String nativeLanguage, String currencyCode, String currencyName, String currencySymbol, String flagPng) {
        this.name = name;
        this.alpha2Code = alpha2Code;
        this.alpha3Code = alpha3Code;
        this.nativeName = nativeName;
        this.region = region;
        this.subRegion = subRegion;
        setLatitude(latitude);
        setLongitude(longitude);
        setArea(area);
        setNumericCode(numericCode);
        this.nativeLanguage = nativeLanguage;
        this.currencyCode = currencyCode;
        this.currencyName = currencyName;
        this.currencySymbol = currencySymbol;
        this.flagPng = flagPng;
    }

    protected Country(Parcel in) {
        name = in.readString();
        alpha2Code = in.readString();
        alpha3Code = in.readString();
        nativeName = in.readString();
        region = in.readString();
        subRegion = in.readString();
        latitude = in.readFloat();
        longitude = in.readFloat();
        area = in.readFloat();
        numericCode = in.readInt();
        nativeLanguage = in.readString();
        currencyCode = in.readString();
        currencyName = in.readString();
        currencySymbol = in.readString();
        flagPng = in.readString();
    }

    public static final Creator<Country> CREATOR = new Creator<Country>() {
        @Override
        public Country createFromParcel(Parcel in) {
            return new Country(in);
        }

        @Override
        public Country[] newArray(int size) {
            return new Country[size];
        }
    };

    private void setLatitude(String latitude) {
        this.latitude = Float.parseFloat(latitude);
    }

    private void setLongitude(String longitude) {
        this.longitude = Float.parseFloat(longitude);
    }

    private void setArea(String area) {
        this.area = Float.parseFloat(area);
    }

    private void setNumericCode(String numericCode) {
        this.numericCode = Integer.valueOf(numericCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(alpha2Code);
        dest.writeString(alpha3Code);
        dest.writeString(nativeName);
        dest.writeString(region);
        dest.writeString(subRegion);
        dest.writeFloat(latitude);
        dest.writeFloat(longitude);
        dest.writeFloat(area);
        dest.writeInt(numericCode);
        dest.writeString(nativeLanguage);
        dest.writeString(currencyCode);
        dest.writeString(currencyName);
        dest.writeString(currencySymbol);
        dest.writeString(flagPng);
    }
}
