package ltd.vblago.taskfp1.util;

import java.util.List;

import ltd.vblago.taskfp1.model.Country;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Query;

public class Retrofit {

    private static final String ENDPOINT = "http://bcmarket.fun";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }


    interface ApiInterface {
        @GET("/countries.php")
        void getInfoOneParameter(@Query("info") String info, Callback<List<String>> callback);

        @GET("/countries.php")
        void getInfoTwoParameters(@Query("info") String info, @Query("param") String param, Callback<List<String>> callback);

        @GET("/countries.php")
        void getCountry(@Query("info") String info, @Query("param") String param, Callback<Country> callback);
    }

    private static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getInfoOneParameter(String info, Callback<List<String>> callback) {
        apiInterface.getInfoOneParameter(info, callback);
    }

    public static void getInfoTwoParameters(String info, String param, Callback<List<String>> callback) {
        apiInterface.getInfoTwoParameters(info, param, callback);
    }

    public static void getCountry(String info, String country, Callback<Country> callback) {
        apiInterface.getCountry(info, country, callback);
    }
}

